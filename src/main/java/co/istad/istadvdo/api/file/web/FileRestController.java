package co.istad.istadvdo.api.file.web;

import co.istad.istadvdo.api.base.Rest;
import co.istad.istadvdo.api.file.FileServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("api/v1/files")
@RequiredArgsConstructor
public class FileRestController {

    private final FileServiceImpl fileService;
    private final Timestamp timestamp;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Rest<?> uploadOne(@RequestPart("file") MultipartFile file) {

        var fileDto = fileService.uploadOne(file);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .message("File has been uploaded!")
                .timestamp(timestamp)
                .data(fileDto)
                .build();
    }

    @PostMapping(value = "upload-all",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Rest<?> uploadAll(@RequestPart("files") List<MultipartFile> files) {

        List<FileDto> fileDtoList = fileService.uploadAll(files);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .message("Files have been uploaded!")
                .timestamp(timestamp)
                .data(fileDtoList)
                .build();
    }

}
