package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlaylistMapStruct {

    Playlist fromPlaylistRequest(PlaylistRequest playlistRequest);

    PlaylistDto toPlaylistDto(Playlist playlist);

}
