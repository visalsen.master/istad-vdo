package co.istad.istadvdo.api.playlist;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Mapper
public interface PlaylistMapper {

    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    @InsertProvider(type = PlaylistProvider.class, method = "buildInsertSql")
    void insert(@Param("playlist") Playlist playlist);

    @Results(id = "playlistResultMap", value = {
            @Result(property = "ownerId", column = "owner_id"),
            @Result(property = "accessCode", column = "access_code"),
            @Result(property = "createdAt", column = "created_at"),
            @Result(property = "updatedAt", column = "updated_at")
    })
    @SelectProvider(type = PlaylistProvider.class, method = "buildSelectByIdSql")
    Optional<Playlist> selectById(@Param("id") Long id);

}
