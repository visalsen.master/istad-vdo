package co.istad.istadvdo.api.playlist;

import org.apache.ibatis.jdbc.SQL;

public class PlaylistProvider {

    public String buildInsertSql() {
        return new SQL() {{
            INSERT_INTO("playlists");
            VALUES("uuid", "#{playlist.uuid}");
            VALUES("title", "#{playlist.title}");
            VALUES("owner_id", "#{playlist.ownerId}");
            VALUES("visibility", "#{playlist.visibility}");
            VALUES("access_code", "#{playlist.accessCode}");
            VALUES("thumbnail", "#{playlist.thumbnail}");
            VALUES("created_at", "#{playlist.createdAt}");
            VALUES("updated_at", "#{playlist.updatedAt}");
            VALUES("status", "#{playlist.status}");
        }}.toString();
    }

    public String buildSelectByIdSql() {
        return new SQL() {{
            SELECT("*");
            FROM("playlists");
            WHERE("id = #{id}");
        }}.toString();
    }

}
