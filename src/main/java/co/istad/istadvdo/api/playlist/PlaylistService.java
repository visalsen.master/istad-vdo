package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;

public interface PlaylistService {

    PlaylistDto createNewPlaylist(PlaylistRequest playlistRequest);

}
