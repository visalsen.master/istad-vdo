package co.istad.istadvdo.api.playlist;

import co.istad.istadvdo.api.playlist.web.PlaylistDto;
import co.istad.istadvdo.api.playlist.web.PlaylistRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PlaylistServiceImpl implements PlaylistService{

    private final PlaylistMapper playlistMapper;
    private final PlaylistMapStruct playlistMapStruct;

    @Override
    public PlaylistDto createNewPlaylist(PlaylistRequest playlistRequest) {

        Playlist playlist = playlistMapStruct.fromPlaylistRequest(playlistRequest);

        // Set up playlist for insert into db
        playlist.setUuid(UUID.randomUUID().toString());
        playlist.setAccessCode("ISTAD2023");
        playlist.setCreatedAt(LocalDateTime.now());
        playlist.setUpdatedAt(LocalDateTime.now());
        playlist.setStatus(true);

        playlistMapper.insert(playlist);

        playlist = playlistMapper.selectById(playlist.getId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Playlist is not existed!",
                        new Throwable("Something went wrong!"))
        );

        return playlistMapStruct.toPlaylistDto(playlist);
    }
}
