package co.istad.istadvdo.api.playlist.web;

import java.time.LocalDateTime;

public record PlaylistDto(
        String uuid,
        String title,
        Long ownerId,
        String visibility,
        String thumbnail,
        LocalDateTime createdAt,
        LocalDateTime updatedAt
) {
}
