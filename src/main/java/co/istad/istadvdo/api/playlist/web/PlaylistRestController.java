package co.istad.istadvdo.api.playlist.web;

import co.istad.istadvdo.api.base.Rest;
import co.istad.istadvdo.api.playlist.PlaylistServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;

@RestController
@RequestMapping("api/v1/playlists")
@RequiredArgsConstructor
public class PlaylistRestController {

    private final PlaylistServiceImpl playlistService;
    private final Timestamp timestamp;

    @PostMapping
    Rest<?> createNewPlaylist(@Valid @RequestBody PlaylistRequest playlistRequest) {

        var playlistDto = playlistService.createNewPlaylist(playlistRequest);

        return Rest.builder()
                .status(true)
                .code(HttpStatus.CREATED.value())
                .timestamp(timestamp)
                .message("Playlist has been created!")
                .data(playlistDto)
                .build();
    }

}
