package co.istad.istadvdo.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Timestamp;

@Configuration
public class DateTimeUtils {

    @Bean
    public Timestamp currentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

}
